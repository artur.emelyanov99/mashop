<?php

// Add orderBy
add_action( 'rest_api_init', 'addOrderby' );
add_action( 'rest_api_init', 'addCategoryFilter' );

function addOrderby(){

	$post_type ="products";
	// Add meta your meta field to the allowed values of the REST API orderby parameter
	add_filter(
		'rest_' . $post_type . '_collection_params',
		function( $params ) {
			$fields = ["rating","title","single_price"];
			foreach ($fields as $key => $value) {
				$params['orderby']['enum'][] = $value;
			}
			return $params;
		},
		10,
		1
	);
	
	// Manipulate query
	add_filter(
		'rest_' . $post_type . '_query',
		function ( $args, $request ) {
			$fields = ["rating","title","single_price"];
			$order_by = $request->get_param( 'orderby' );
			if ( isset( $order_by ) && in_array($order_by, $fields)) {
				$args['meta_key'] = $order_by;
				$args['orderby']  = 'meta_value';
			}
			return $args;
		},
		10,
		2
	);
} // addOrderby function ends. 

// Add addCategoryFilter

function addCategoryFilter () {

	add_filter( 
		'rest_products_query',
		function ( $valid_vars ) {
    		return array_merge( $valid_vars, array( 'category', 'meta_query' ) );
	} );

	add_filter( 
		'rest_products_query',
		function( $args, $request ) {
			$category   = $request->get_param( 'category' );

			if ( ! empty( $category ) ) {
				$args['meta_query'] = array(
					array(
						'key'     => 'category',
						'value'   => $category,
						'compare' => '=',
					)
				);      
			}
			return $args;
		}, 
		10, 
		2 
	);
}	// addCategoryFilter function ends. 